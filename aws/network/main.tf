7provider "aws" {
  #Especificar region
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

#setup remote terraform.state
terraform {
 backend "s3" {
   encrypt = true
   bucket  = "terraform-remote-state-farmacias/uat02/network/"
   region  = "us-east-1"
   key     = "terraform.tfstate"
 }
}

### Parametros de la VPC ####
module "vpc" {
  source          = "./src/"
  name            = "UAT02"
  cidr            = "10.9.0.0/16"
  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["10.9.1.0/24", "10.9.2.0/24", "10.9.3.0/24"]
  public_subnets  = ["10.9.4.0/24", "10.9.5.0/24", "10.9.6.0/24"]
  environment     = "UAT02"
  domain          = "uat02.internal"
}
