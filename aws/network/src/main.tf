
######
# VPC
######
resource "aws_vpc" "this" {
  cidr_block       = "${var.cidr}"
  instance_tenancy = "default"
  enable_dns_hostnames = "true"
  enable_dns_support =   "true"

  tags = "${
    map(
     "Name", "${var.name}",
     "kubernetes.io/cluster/eks_uat02", "owned",
     "Environment", "${var.environment}"
    )
  }"
}

###################
# Internet Gateway
###################
resource "aws_internet_gateway" "this" {
  vpc_id = "${aws_vpc.this.id}"

  tags = {
    Name = "IGW"
    Environment = "${var.environment}"
  }
}

################
# Publiс routes
################
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.this.id}"

  tags = {
    Name = "PUBLIC"
    Environment = "${var.environment}"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.this.id}"

  timeouts {
    create = "5m"
  }
}

#################
# Private routes
#################
 resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.this.id}"

  tags = {
    Name = "PRIVATE"
    Environment = "${var.environment}"
  }
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat_gw.id}"

  timeouts {
    create = "5m"
  }
}

################
# Public subnet
################
resource "aws_subnet" "public" {
  count = "${length(var.public_subnets)}"

  vpc_id                  = "${aws_vpc.this.id}"
  cidr_block              = "${var.public_subnets[count.index]}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  tags = "${
    map(
     "Name", "Public",
     "kubernetes.io/cluster/eks_uat02", "shared",
     "Environment", "${var.environment}",
     "kubernetes.io/role/elb", "1"
    )
  }"
}



#################
# Private subnet
#################
resource "aws_subnet" "private" {
  count = "${length(var.private_subnets)}"

  vpc_id            = "${aws_vpc.this.id}"
  cidr_block        = "${var.private_subnets[count.index]}"
  availability_zone = "${element(var.azs, count.index)}"
  tags = {
      Name = "Private"
      Environment = "${var.environment}"
  }
}

##########################
# Route table association
##########################
resource "aws_route_table_association" "private" {
  count = "${length(var.private_subnets)}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

resource "aws_route_table_association" "public" {
  count = "${length(var.public_subnets)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

#############
# NAT GATEWAY
#############

resource "aws_eip" "nat" {
  vpc = true

  tags = {
    Name = "IP NAT UAT02"
    Environment = "${var.environment}"
  }
}

resource "aws_nat_gateway" "nat_gw" {

  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.public.2.id}"

  tags = {
    Name = "UAT02"
    Environment = "${var.environment}"
  }
}

##################
# SECURITY GROUP #
##################
resource "aws_security_group" "interno" {
  name        = "sg_interno"
  description = "Permite todo el trafico interno dentro de la vpc"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.cidr}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  tags = "${
    map(
     "Name", "sg_interno",
     "kubernetes.io/cluster/${var.cluster_name}", "owned",
     "eks", "yes",
     "Environment", "${var.environment}"
    )
  }"
  vpc_id = "${aws_vpc.this.id}"
}


resource "aws_security_group" "cluster" {
  name        = "sg_cluster"
  description = "Permite acceso al cluster"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  tags = {
    Name = "sg_cluster"
    eks  = "yes"
  }
  vpc_id = "${aws_vpc.this.id}"
}

resource "aws_security_group" "bastion" {
  name        = "sg_bastion"
  description = "Permite acceso al bastion"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  tags = {
    Name = "sg_bastion"
    Environment = "${var.environment}"
  }
  vpc_id = "${aws_vpc.this.id}"
}

resource "aws_security_group" "rds_externo" {
  name        = "sg_rds_externo"
  description = "Permite acceso al servicio las bds"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    cidr_blocks = ["200.37.217.53/32"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  tags = {
    Name = "sg_rds_externo"
    Environment = "${var.environment}"
  }
  vpc_id = "${aws_vpc.this.id}"
}

resource "aws_security_group" "sg_jenkins" {
  name        = "sg_jenkins"
  description = "Permite acceso al servidor jenkins"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  tags = {
    Name = "sg_jenkins"
    Environment = "${var.environment}"
  }
  vpc_id = "${aws_vpc.this.id}"
}

#########
## DNS ##
#########
resource "aws_route53_zone" "this" {
  name = "${var.domain}"

  vpc {
    vpc_id = "${aws_vpc.this.id}"
  }
  tags = {
    Environment = "${var.environment}"
  }
}

##########
## DHCP ##
##########
resource "aws_vpc_dhcp_options" "this" {
  domain_name         = "${var.domain}"
  domain_name_servers = ["AmazonProvidedDNS"]
  tags = {
    Name              = "${var.domain}"
    Environment       = "${var.environment}"
  }
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = "${aws_vpc.this.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.this.id}"
}
