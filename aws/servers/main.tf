provider "aws" {
  #Especificar region
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

#setup remote terraform.state
terraform {
 backend "s3" {
   encrypt = true
   bucket  = "terraform-remote-state-farmacias/uat02/servers/"
   region  = "us-east-1"
   key     = "terraform.tfstate"
 }
}

module "servers" {
  source          = "./src/"
  environment     = "UAT02"
  key_name        = "uat-inkafarma"

  name            = "Bastion-UAT02"
  num_instances   = 1
  image_server    = "ami-08501524e1ec5190b"
  type            = "t2.micro"
  volume-type     = "gp2"
  volume-size     = 20


  master-name          = "JK-MASTER01-UAT02"
  image_jenkins_master = "ami-03c3944bf94c5a2f1"
  type_master          = "t2.large"
  volume-type_master   = "gp2"
  volume-size_master   = "50"
  dns_name_master      = "jk-master01"



  slave-name          = "JK-SLAVE01-UAT02"
  image_jenkins_slave = "ami-03a947cf4c8ef5020"
  type_slave          = "t2.large"
  volume-type_slave   = "gp2"
  volume-size_slave   = "100"
  dns_name_slave      = "jk-slave01"

}
