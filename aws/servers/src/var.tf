variable "name" {
  default = ""
}
variable "image_server" {
  default = ""
}
variable "type" {
  default = ""
}


variable "volume-type" {
  default = ""
}
variable "volume-size" {
  default = ""
}
variable "public_subnets" {
  default = []
}
variable "sg_interno" {
  default = []
}

variable "sg_jenkins" {
  default = []
}

variable "sg_bastion" {
  default = []
}
variable "num_instances" {
  default = 0
}

variable "key_name" {
  default = ""
}
variable "eks_cluster_name" {
  default = ""
}

variable "image_jenkins_master" {
  default = ""
}

variable "volume-type_master" {
  default = ""
}

variable "volume-size_master" {
  default = ""
}

variable "image_jenkins_slave" {
  default = ""
}

variable "type_slave" {
  default = ""
}

variable "type_master" {
  default = ""
}

variable "volume-type_slave" {
  default = ""
}

variable "volume-size_slave" {
  default = ""
}

variable "slave-name" {
  default = ""
}

variable "master-name" {
  default = ""
}

variable "environment" {
  default = ""
}

variable "dns_name_master" {
  default = ""
}

variable "dns_name_slave" {
  default = ""
}
