### Obtener region actual ###
data "aws_region" "current" {}

### Obtener vpc ###
data "aws_vpc" "selected" {
  tags = {
    Name = "UAT02"
  }
}

### Obtener subnets a usar ###
data "aws_subnet_ids" "selected" {
  vpc_id = "${data.aws_vpc.selected.id}"
  tags = {
    Name = "Public"
    Environment = "${var.environment}"
  }
}

### Obtener SG interno ###
data "aws_security_groups" "interno" {
  tags = {
    Name = "sg_interno"
    Environment = "${var.environment}"

  }
}

### Obtener SG interno ###
data "aws_security_groups" "bastion" {
  tags = {
    Name = "sg_bastion"
    Environment = "${var.environment}"
  }
}

### Obtener SG jenkins ###
data "aws_security_groups" "jenkins" {
  tags = {
    Name = "sg_jenkins"
    Environment = "${var.environment}"
  }
}

### Obtener dns zone ###
data "aws_route53_zone" "selected" {
  name         = "uat02.internal."
  private_zone = true
  tags = {
    Environment = "UAT02"
  }
}

resource "aws_instance" "bastion" {
  ami                    = "${var.image_server}"
  instance_type          = "${var.type}"
  vpc_security_group_ids = ["sg-09b0f1c8f31987960"]
  subnet_id              = "subnet-0177b1789e9fdc830"
  key_name               = "${var.key_name}"
  iam_instance_profile   = "Jenkins-to-AWS"
/**  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname bastion01.inka.internal
              yum install mysql -y
              mkdir /home/centos/bin
              curl -o /home/centos/bin/kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/kubectl
              chmod +x /home/centos/bin/kubectl
              echo 'export PATH=/home/centos/bin:$PATH' >> /home/centos/.bashrc
              curl -o /home/centos/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/aws-iam-authenticator
              chmod +x /home/centos/bin/aws-iam-authenticator
              chown -R centos:centos /home/centos/bin/
              curl -O https://bootstrap.pypa.io/get-pip.py
              python get-pip.py
              /usr/bin/pip install awscli --upgrade
              echo '[default]' > /home/centos/.aws/config
              echo 'region = us-east-1' >> /home/centos/.aws/config
              chown -R centos:centos /home/centos/.aws/
              /usr/bin/aws eks update-kubeconfig --name "${var.eks_cluster_name}"
              EOF**/

  root_block_device {
    delete_on_termination = "true"
    volume_type = "${var.volume-type}"
    volume_size = "${var.volume-size}"

  }
  tags = {
    Name = "${var.name}"
    Environment = "${var.environment}"
  }

  volume_tags = {
    Name = "${var.name}"
    Environment = "${var.environment}"
  }
}

resource "aws_eip" "bastion" {
  vpc = true
  tags = {
    Name = "IP BASTION"
    Environment = "${var.environment}"
  }
}

resource "aws_eip_association" "bastion" {
  instance_id   = "${aws_instance.bastion.id}"
  allocation_id = "${aws_eip.bastion.id}"
}

resource "aws_instance" "jenkis-master" {
  ami                    = "${var.image_jenkins_master}"
  instance_type          = "${var.type_master}"
  vpc_security_group_ids = ["sg-09b0f1c8f31987960", "sg-0c3755676fced0034", "sg-0d08adf1695f5bcd2"]
  subnet_id              = "subnet-0177b1789e9fdc830"
  key_name               = "${var.key_name}"

  root_block_device {
    delete_on_termination = "true"
    volume_type = "${var.volume-type_master}"
    volume_size = "${var.volume-size_master}"

  }
  tags = {
    Name = "${var.master-name}"
    Environment = "${var.environment}"
  }

  volume_tags = {
    Name = "${var.master-name}"
    Environment = "${var.environment}"
  }
}

resource "aws_eip" "master" {
  vpc = true
  tags = {
    Name = "IP JENKINS MASTER"
    Environment = "${var.environment}"
  }
}

resource "aws_eip_association" "master" {
  instance_id   = "${aws_instance.jenkis-master.id}"
  allocation_id = "${aws_eip.master.id}"
}


resource "aws_instance" "jenkis-slave" {
  ami                    = "${var.image_jenkins_slave}"
  instance_type          = "${var.type_slave}"
  vpc_security_group_ids = ["sg-09b0f1c8f31987960", "sg-0c3755676fced0034"]
  subnet_id              = "subnet-0177b1789e9fdc830"
  key_name               = "${var.key_name}"
  iam_instance_profile   = "Jenkins-to-AWS"

  root_block_device {
    delete_on_termination = "true"
    volume_type = "${var.volume-type_slave}"
    volume_size = "${var.volume-size_slave}"

  }
  tags = {
    Name        = "${var.slave-name}"
    Environment = "${var.environment}"
  }

  volume_tags = {
    Name        = "${var.slave-name}"
    Environment = "${var.environment}"
  }
}

resource "aws_eip" "slave" {
  vpc = true
  tags = {
    Name        = "IP JENKINS slave"
    Environment = "${var.environment}"
  }
}

resource "aws_eip_association" "slave" {
  instance_id   = "${aws_instance.jenkis-slave.id}"
  allocation_id = "${aws_eip.slave.id}"
}

## Registros DNS ##
resource "aws_route53_record" "jenkins_master" {
  zone_id = "${data.aws_route53_zone.selected.id}"
  name    = "${var.dns_name_master}.${data.aws_route53_zone.selected.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.jenkis-master.private_ip}"]
}

resource "aws_route53_record" "jenkins_slave" {
  zone_id = "${data.aws_route53_zone.selected.id}"
  name    = "${var.dns_name_slave}.${data.aws_route53_zone.selected.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.jenkis-slave.private_ip}"]
}
