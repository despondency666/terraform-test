#setup remote terraform.state

terraform {
  backend "gcs" {
    bucket  = "cicd"
    prefix  = "terraform/inkafarma/ci01"
  }
}

provider "google" {
  credentials = "auth/inkafarma-dev-50838f71ab8e.json"
  project     = var.project
  region      = var.region
}


### Parametros de la VPC ####

module "network" {
  source = "../../../modules/inkafarma/network"
  
  network_name = "CI01"
}

### Parametros de BD ####

module "db" {
    source = "../../../modules/inkafarma/database"


}